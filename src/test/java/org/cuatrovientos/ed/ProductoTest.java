package org.cuatrovientos.ed;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ProductoTest {
	
	@BeforeEach
	void setUp() throws Exception {
	}

	@Test
	public void ProductoTest() {
		assertEquals(new Producto("pan", 0.50f, 1), "Test de Producto");
	}
	
	@Test
	public void precioTotal() {
		assertEquals(new Producto("pan", 0.50f, 4), "Test del precio de un producto");
	}

		
	}
	
