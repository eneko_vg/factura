package org.cuatrovientos.ed;

import java.util.ArrayList;

public class Factura {
	ArrayList<Producto> Productos= new ArrayList<Producto>();
	
	public void meterProducto(ArrayList<Producto> Productos) {
		this.Productos=Productos;
	}
	
	public float totalFactura() {
		float factura = 0;
		for (int i=0; i<Productos.size(); i++) {
			factura= factura+Productos<Producto[i].precioTotal()>;
		}
		return factura;
	}

	public float aplicarIVA(float IVA) {
		float precioConIVA;
		precioConIVA=this.totalFactura()*IVA;
		
		return precioConIVA;
	}
}
