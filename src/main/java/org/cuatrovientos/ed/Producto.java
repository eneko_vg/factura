package org.cuatrovientos.ed;

public class Producto {

	String nombre;
	float precio;
	int cantidad;
	
	public Producto(String nombre, float precio, int cantidad) {
		this.nombre=nombre;
		this.precio=precio;
		this.cantidad=cantidad;
	}
	
	public float precioTotal() {
		float precioTotal;
		precioTotal = this.precio*this.cantidad;
		return precioTotal;
	}

	@Override
	public String toString() {
		return "Producto [nombre=" + nombre + ", precio=" + precio + ", cantidad=" + cantidad + "]";
	}
		
}
